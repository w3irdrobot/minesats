import db from "../src/pages/api/_db";

async function teardown(): Promise<void> {
  await db.migrate.rollback(null, true);
  await db.destroy();
}

export default teardown;
