import db from "../src/pages/api/_db";

async function setup(): Promise<void> {
  await db.migrate.latest();
}

export default setup;
