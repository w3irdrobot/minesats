import secp256k1 from "secp256k1";
import crypto from "crypto";

export const genKeyPair = (): [Uint8Array, Uint8Array] => {
  let privKey: Uint8Array;
  do {
    privKey = Uint8Array.from(crypto.randomBytes(32));
  } while (!secp256k1.privateKeyVerify(privKey));
  const pubKey = secp256k1.publicKeyCreate(privKey);
  return [privKey, pubKey];
};
