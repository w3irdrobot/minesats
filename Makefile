VERSION_TAG := $(shell git describe --tags | sed -e 's/-[[:digit:]]\+-g/+/')

build:
	sed -i.bak -e 's/^NEXT_PUBLIC_VERSION_TAG=.*/NEXT_PUBLIC_VERSION_TAG=${VERSION_TAG}/' .env.docker
	sed -i.bak -e 's/^VERSION_TAG=.*/VERSION_TAG=${VERSION_TAG}/' bot/.env.docker
	docker-compose build
	mv .env.docker.bak .env.docker
	mv bot/.env.docker.bak bot/.env.docker

start:
	docker-compose up -d
