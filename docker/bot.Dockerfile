FROM golang:1.19

WORKDIR /app
COPY . .
COPY bot/.env.docker bot/.env


WORKDIR /app/bot/
RUN go install -v

CMD go run .
