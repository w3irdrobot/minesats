exports.up = async function (knex) {
  await knex.schema.createView("miners", (view) => {
    view.columns(["name"]);
    view.as(knex.distinct("name").from("blockrewards"));
  });

  await knex.raw(`
    CREATE OR REPLACE VIEW top_miners_144 AS (
      WITH
        prev_rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 144
          OFFSET 144
        ),
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 144
        ),
        sum_prev_rewards AS (
          SELECT name, SUM(amount) AS amount FROM prev_rewards GROUP BY name
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT m.name, COALESCE(r.amount, 0) amount, COALESCE(pr.amount, 0) AS prev_amount, (r.amount/pr.amount::float) - 1 AS delta
      FROM miners m
      LEFT JOIN sum_rewards r ON m.name = r.name
      LEFT JOIN sum_prev_rewards pr ON pr.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE OR REPLACE VIEW top_miners_1008 AS (
      WITH
        prev_rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 1008
          OFFSET 1008
        ),
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 1008
        ),
        sum_prev_rewards AS (
          SELECT name, SUM(amount) AS amount FROM prev_rewards GROUP BY name
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT m.name, COALESCE(r.amount, 0) AS amount, COALESCE(pr.amount, 0) AS prev_amount, (r.amount/pr.amount::float) - 1 AS delta
      FROM miners m
      LEFT JOIN sum_rewards r ON m.name = r.name
      LEFT JOIN sum_prev_rewards pr ON pr.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE OR REPLACE VIEW top_miners_4320 AS (
      WITH
        prev_rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 4320
          OFFSET 4320
        ),
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 4320
        ),
        sum_prev_rewards AS (
          SELECT name, SUM(amount) AS amount FROM prev_rewards GROUP BY name
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT m.name, COALESCE(r.amount, 0) AS amount, COALESCE(pr.amount, 0) AS prev_amount, (r.amount/pr.amount::float) - 1 AS delta
      FROM miners m
      LEFT JOIN sum_rewards r ON m.name = r.name
      LEFT JOIN sum_prev_rewards pr ON pr.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE OR REPLACE VIEW top_miners_total AS (
      WITH
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT m.name, COALESCE(r.amount, 0) AS amount
      FROM miners m
      LEFT JOIN sum_rewards r ON m.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE OR REPLACE VIEW top_miners AS (
      SELECT
        tmtotal.name AS name,
        tmtotal.amount AS amount_total,
        tm4320.amount AS amount_4320,
        tm4320.prev_amount AS prev_amount_4320,
        tm4320.delta AS delta_4320,
        tm1008.amount AS amount_1008,
        tm1008.prev_amount AS prev_amount_1008,
        tm1008.delta AS delta_1008,
        tm144.amount AS amount_144,
        tm144.prev_amount AS prev_amount_144,
        tm144.delta AS delta_144
      FROM miners m
      LEFT JOIN top_miners_total tmtotal ON m.name = tmtotal.name
      LEFT JOIN top_miners_4320 tm4320 ON m.name = tm4320.name
      LEFT JOIN top_miners_1008 tm1008 ON m.name = tm1008.name
      LEFT JOIN top_miners_144  tm144  ON m.name = tm144.name
    )
  `);
};

exports.down = async function (knex) {
  await knex.schema.dropView("top_miners");
  await knex.schema.dropView("top_miners_total");
  await knex.schema.dropView("top_miners_4320");
  await knex.schema.dropView("top_miners_1008");
  await knex.schema.dropView("top_miners_144");
  await knex.schema.dropView("miners");

  await knex.raw(`
    CREATE VIEW top_miners_144 AS (
      WITH
        prev_rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 144
          OFFSET 144
        ),
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 144
        ),
        sum_prev_rewards AS (
          SELECT name, SUM(amount) AS amount FROM prev_rewards GROUP BY name
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT p.name, COALESCE(r.amount, 0) amount, COALESCE(pr.amount, 0) AS prev_amount, (r.amount/pr.amount) - 1 AS delta
      FROM players p
      LEFT JOIN sum_rewards r ON p.name = r.name
      LEFT JOIN sum_prev_rewards pr ON pr.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE VIEW top_miners_1008 AS (
      WITH
        prev_rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 1008
          OFFSET 1008
        ),
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 1008
        ),
        sum_prev_rewards AS (
          SELECT name, SUM(amount) AS amount FROM prev_rewards GROUP BY name
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT p.name, COALESCE(r.amount, 0) AS amount, COALESCE(pr.amount, 0) AS prev_amount, (r.amount/pr.amount) - 1 AS delta
      FROM players p
      LEFT JOIN sum_rewards r ON p.name = r.name
      LEFT JOIN sum_prev_rewards pr ON pr.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE VIEW top_miners_4320 AS (
      WITH
        prev_rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 4320
          OFFSET 4320
        ),
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
          LIMIT 4320
        ),
        sum_prev_rewards AS (
          SELECT name, SUM(amount) AS amount FROM prev_rewards GROUP BY name
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT p.name, COALESCE(r.amount, 0) AS amount, COALESCE(pr.amount, 0) AS prev_amount, (r.amount/pr.amount) - 1 AS delta
      FROM players p
      LEFT JOIN sum_rewards r ON p.name = r.name
      LEFT JOIN sum_prev_rewards pr ON pr.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE VIEW top_miners_total AS (
      WITH
        rewards AS (
          SELECT name, amount FROM blockrewards br
          JOIN messages m ON m.id = br.id
          ORDER BY m.ts DESC
        ),
        sum_rewards AS (
          SELECT name, SUM(amount) AS amount FROM rewards GROUP BY name
        )
      SELECT p.name, COALESCE(r.amount, 0) AS amount
      FROM players p
      LEFT JOIN sum_rewards r ON p.name = r.name
      ORDER BY amount DESC, name ASC
    )
  `);

  await knex.raw(`
    CREATE VIEW top_miners AS (
      SELECT
        tmtotal.name AS name,
        tmtotal.amount AS amount_total,
        tm4320.amount AS amount_4320,
        tm4320.prev_amount AS prev_amount_4320,
        tm4320.delta AS delta_4320,
        tm1008.amount AS amount_1008,
        tm1008.prev_amount AS prev_amount_1008,
        tm1008.delta AS delta_1008,
        tm144.amount AS amount_144,
        tm144.prev_amount AS prev_amount_144,
        tm144.delta AS delta_144
      FROM players p
      LEFT JOIN top_miners_total tmtotal ON p.name = tmtotal.name
      LEFT JOIN top_miners_4320 tm4320 ON p.name = tm4320.name
      LEFT JOIN top_miners_1008 tm1008 ON p.name = tm1008.name
      LEFT JOIN top_miners_144  tm144  ON p.name = tm144.name
    )
  `);
};
