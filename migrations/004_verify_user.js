exports.up = async function (knex) {
  await knex.schema.alterTable("users", (table) => {
    table
      .string("verify_token", 48)
      .notNull()
      .unique()
      .defaultTo(
        knex.raw(
          `'minesats.gg/' || LEFT(encode(gen_random_uuid()::text::bytea, 'base64'), 16)`
        )
      );
  });
};

exports.down = async function (knex) {
  await knex.schema.alterTable("users", (table) => {
    table.dropColumn("verify_token");
  });
};
