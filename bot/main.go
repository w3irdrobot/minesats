package main

import (
	"database/sql"
	"fmt"
	"log"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/namsral/flag"
)

var (
	Token          string
	ChannelID      string
	GuildID        string
	FirstMessageID string
	DatabaseHost   string
	DatabasePort   int
	DatabaseUser   string
	DatabasePw     string
	DatabaseName   string
	dg             *discordgo.Session
	db             *sql.DB
)

type Message struct {
	ID        string
	Timestamp int64
	Content   string
}

type BlockReward struct {
	MID    string
	Name   string
	Amount int64
}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	flag.StringVar(&Token, "bot_token", "", "Discord bot token")
	flag.StringVar(&ChannelID, "channel_id", "", "ID of channel where chat messages are sent to")
	flag.StringVar(&GuildID, "guild_id", "", "ID of the guild of the channel (for roles lookup)")
	flag.StringVar(&FirstMessageID, "first_message_id", "", "ID of first message in channel")
	flag.StringVar(&DatabaseHost, "database_host", "localhost", "Database host")
	flag.IntVar(&DatabasePort, "database_port", 5432, "Database port")
	flag.StringVar(&DatabaseUser, "database_user", "", "Database username")
	flag.StringVar(&DatabasePw, "database_pw", "", "Database password")
	flag.StringVar(&DatabaseName, "database_name", "", "Database name")
	flag.Parse()
}

func initBot() *discordgo.Session {
	dg, err := discordgo.New(Token)
	if err != nil {
		log.Fatal("error creating discord session: ", err)
	}
	dg.AddHandler(func(s *discordgo.Session, event *discordgo.Ready) {
		log.Println("Logged in as", event.User.Username)
	})
	err = dg.Open()
	if err != nil {
		log.Fatal("error opening connection to discord: ", err, " -- Is your token correct?")
	}
	return dg
}

func initDb() *sql.DB {
	connStr := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?sslmode=disable", DatabaseName, DatabasePw, DatabaseHost, DatabasePort, DatabaseName)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	return db
}

func pipeExistingMessages(messageChan chan Message, lastMessageId *string) {
	rows, err := db.Query(`SELECT * FROM messages ORDER BY ts ASC`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var parsed int = 0
	for rows.Next() {
		var id string
		var ts int64
		var content string
		rows.Scan(&id, &ts, &content)
		*lastMessageId = id
		messageChan <- Message{id, ts, content}
		parsed++
	}
	log.Printf("Read %d existing messages", parsed)
}

func pipeFromChannel(messageChan chan Message, lastMessageId string) {
	for {
		messages, err := dg.ChannelMessages(ChannelID, 100, "", lastMessageId, "")
		if err != nil {
			log.Fatal(err)
		}
		// sort by timestamp
		sort.Slice(messages, func(i, j int) bool {
			return messages[i].Timestamp.Unix() < messages[j].Timestamp.Unix()
		})
		mlen := len(messages)
		log.Printf("Received %d new messages", mlen)
		for _, m := range messages {
			content := m.Content
			if len(m.Embeds) > 0 {
				content = m.Embeds[0].Description
			}
			msg := Message{m.ID, m.Timestamp.Unix(), content}
			lastMessageId = m.ID
			_, err := db.Exec(`INSERT INTO messages(id, ts, content) VALUES($1, to_timestamp($2), $3) ON CONFLICT DO NOTHING`, m.ID, m.Timestamp.Unix(), msg.Content)
			if err != nil {
				log.Fatal(err)
			}
			messageChan <- msg
		}
		log.Printf("Inserted %d new messages", mlen)
		if len(messages) < 100 {
			wait := 3 * time.Second
			log.Printf("Waiting %d seconds now ...", wait/1e9)
			time.Sleep(wait)
		}
	}
}

func pipeMessages(messageChan chan Message) {
	LastMessageID := FirstMessageID
	pipeExistingMessages(messageChan, &LastMessageID)
	pipeFromChannel(messageChan, LastMessageID)
}

func parseBlockRewardMessages(messageChan chan Message) {
	re1 := regexp.MustCompile("^Winner! A block reward of ([0-9,.]+)\\.0 sats has been won by (.*)!$")
	re2 := regexp.MustCompile("^<@&([0-9]+)> has won ([0-9,.]+)\\.0 sats!$")
	for m := range messageChan {
		var name string
		var amount int
		var err error

		match1 := re1.FindStringSubmatch(m.Content)
		if match1 != nil {
			amount, err = strconv.Atoi(strings.Replace(match1[1], ",", "", 1))
			if err != nil {
				log.Fatal(err)
			}
			name = match1[2]
		}

		match2 := re2.FindStringSubmatch(m.Content)
		if match2 != nil {
			amount, err = strconv.Atoi(strings.Replace(match2[2], ",", "", 1))
			if err != nil {
				log.Fatal(err)
			}
			name = match2[1]
		}

		if _, err := strconv.ParseInt(name, 10, 64); err == nil {
			name = roleName(name)
		}

		if match1 != nil || match2 != nil {
			_, err := db.Exec(`INSERT INTO blockrewards(id, name, amount) VALUES($1, $2, $3) ON CONFLICT DO NOTHING`, m.ID, name, amount)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func parsePlayerEvents(messageChan chan Message) {
	loginRe := regexp.MustCompile("^<:JPG:1068536196843704440> \\*\\*(.*)?\\*\\* joined the game$")
	logoutRe := regexp.MustCompile("^:smiling_face_with_tear: \\*\\*(.*)?\\*\\* left the game$")
	verifyRe := regexp.MustCompile("^:speech_balloon: <\\*\\*(.*)?\\*\\*>:.*(minesats\\.gg/[a-zA-Z0-9]{16}).*$")
	for m := range messageChan {
		var name string

		match1 := loginRe.FindStringSubmatch(m.Content)
		if match1 != nil {
			name = match1[1]
			_, err := db.Exec(`INSERT INTO player_events(id, name, type) VALUES($1, $2, $3) ON CONFLICT DO NOTHING`, m.ID, name, "login")
			if err != nil {
				log.Fatal(err)
			}
		}

		match2 := logoutRe.FindStringSubmatch(m.Content)
		if match2 != nil {
			name = match2[1]
			_, err := db.Exec(`INSERT INTO player_events(id, name, type) VALUES($1, $2, $3) ON CONFLICT DO NOTHING`, m.ID, name, "logout")
			if err != nil {
				log.Fatal(err)
			}
		}

		match3 := verifyRe.FindStringSubmatch(m.Content)
		if match3 != nil {
			name = match3[1]
			verifyToken := match3[2]
			// Need to update verify token after it was used to prevent other users replaying the token
			_, err := db.Exec(`UPDATE users SET player_name = $1, verify_token = DEFAULT WHERE verify_token = $2`, name, verifyToken)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func roleName(id string) string {
	roles, err := dg.GuildRoles(GuildID)
	if err != nil {
		log.Fatal(err)
	}
	for _, role := range roles {
		if role.ID == id {
			return role.Name
		}
	}
	return ""
}

func fanOut(messageChan chan Message, fns ...func(chan Message)) {
	chans := []chan Message{}
	for _, fn := range fns {
		c := make(chan Message)
		chans = append(chans, c)
		go fn(c)
	}
	for m := range messageChan {
		for _, c := range chans {
			c <- m
		}
	}
}

func main() {
	dg = initBot()
	defer dg.Close()
	db = initDb()
	defer db.Close()

	messageChan := make(chan Message)
	go pipeMessages(messageChan)
	fanOut(
		messageChan,
		parseBlockRewardMessages,
		parsePlayerEvents,
	)
}
