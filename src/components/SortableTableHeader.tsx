import { useEffect, useState } from "react";

import SortArrow from "@components/SortArrow";

type SortableTableHeaderProps = {
  sortKey: string;
  currentSortKey: string;
  sortOrder: "asc" | "desc";
  setSortKey: React.Dispatch<React.SetStateAction<string>>;
  setSortOrder: React.Dispatch<React.SetStateAction<string>>;
  label: string;
  [key: string]: any;
};

export default function SortableTableHeader({
  label,
  sortKey,
  currentSortKey,
  sortOrder,
  setSortKey,
  setSortOrder,
  ...props
}: SortableTableHeaderProps) {
  const [iconOrder, setIconOrder] = useState<"asc" | "desc">("desc");
  useEffect(() => {
    if (sortKey === currentSortKey) setIconOrder(sortOrder);
  }, [sortKey, currentSortKey, sortOrder]);

  return (
    <th
      {...props}
      onClick={() => {
        setSortKey(sortKey);
        // only change sort order if we already sort by this column
        if (currentSortKey === sortKey)
          setSortOrder((prevOrder) => (prevOrder === "asc" ? "desc" : "asc"));
        // else use sort order as already shown by icon
        else setSortOrder(iconOrder);
      }}
    >
      <div className="flex flex-row justify-center">
        <span className="m-auto">{label}</span>
        <SortArrow order={iconOrder} active={sortKey === currentSortKey} />
      </div>
    </th>
  );
}
