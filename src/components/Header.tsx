import { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useAuth } from "@context/AuthContext";

type DropdownProps = { show: boolean };

export default function Header() {
  const { user, loading, setUser } = useAuth();
  const [showDropdown, setShowDropdown] = useState(false);

  const logout = () => {
    fetch("/api/logout", { method: "POST" }).finally(() => setUser(null));
  };

  const Dropdown = ({ show }: DropdownProps) => {
    if (!show) return null;
    return (
      <div className="relative">
        <div className="z-1 absolute right-0 bg-onyx py-3 px-14 border rounded">
          <Link href={`/u/${user.display_name}`}>
            <div className="text-button">profile</div>
          </Link>
          <div className="text-button" onClick={logout}>
            logout
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="flex justify-between px-1 py-2">
      <div className="flex flex-row">
        <Link href="/">
          <Image
            src="/minesats.png"
            alt="minesats"
            width={151}
            height={30}
            priority={true}
          />
        </Link>
        <div className="items-end text-xs pb-1 hidden sm:flex">
          {process.env.NEXT_PUBLIC_VERSION_TAG || "v0.1.0"}
        </div>
      </div>
      {loading ? null : user?.display_name ? (
        <div className="flex items-center">
          <div className="flex flex-col">
            <div
              className="text-button max-w-[12ch] text-ellipsis overflow-hidden"
              onClick={() => setShowDropdown((show) => (show ? false : true))}
            >
              @{user.display_name}
            </div>
            <Dropdown show={showDropdown} />
          </div>
        </div>
      ) : (
        <div className="flex flex-row">
          <Link href="/login" className="mx-1 ">
            <button className="gray-button">login</button>
          </Link>
          <Link href="/signup">
            <button className="yellow-button">sign up</button>
          </Link>
        </div>
      )}
    </div>
  );
}
