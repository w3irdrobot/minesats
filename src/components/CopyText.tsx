import { useState } from "react";
import copy from "clipboard-copy";

import Thumb from "@svg/thumbs-up.svg";

export default function CopyText({ text }) {
  const [copied, setCopied] = useState(false);
  const onClick = () => {
    copy(text);
    setCopied(true);
    setTimeout(() => setCopied(false), 1500);
  };

  return (
    <>
      <input
        placeholder={text}
        disabled
        className="border-neutral-400 border box-border bg-black rounded-md px-3 py-1 rounded-tr-none rounded-br-none caret-transparent"
      />
      <div>
        <button
          className="yellow-button h-full rounded-tl-none rounded-bl-none"
          onClick={onClick}
        >
          {copied ? <Thumb width={18} height={18} /> : "copy"}
        </button>
      </div>
    </>
  );
}
