import { useEffect, useState } from "react";

import { TopMiner, TopMinerSortKey } from "@/api/topMiners";
import RankDiff from "@components/RankDiff";
import SortableTableHeader from "@components/SortableTableHeader";

const formatPercentage = (val) => `${val < 0 ? "" : "+"}${val.toFixed(2)}%`;

const colorizeVal = (val) => {
  return val > 0 ? "text-green-400" : val < 0 ? "text-red-400" : "";
};

const formatter = new Intl.NumberFormat(`en`);

const borderStyle = "border border-white p-1 text-xs sm:text-sm md:text-base";

const minerToRow = (miner: TopMiner, key: number) => {
  const rankDiff = miner.prevRank - miner.rank;
  const d144 = miner.delta144 * 100;
  const d1008 = miner.delta1008 * 100;
  const d4320 = miner.delta4320 * 100;
  const d144f = miner.delta144 ? formatPercentage(d144) : "n/a";
  const d1008f = miner.delta1008 ? formatPercentage(d1008) : "n/a";
  const d4320f = miner.delta4320 ? formatPercentage(d4320) : "n/a";
  return (
    <tr key={key}>
      <td className={borderStyle}>
        <div className="flex flex-row justify-end">
          <span>{miner.rank}</span>
          {miner.prevRank !== -1 ? <RankDiff value={rankDiff} /> : null}
        </div>
      </td>
      <td className={borderStyle}>{miner.name}</td>
      <td className={borderStyle + " text-right"}>
        ⚡{formatter.format(miner.amount144)} (
        <span className={colorizeVal(d144)}>{d144f}</span>)
      </td>
      <td className={borderStyle + " text-right"}>
        ⚡{formatter.format(miner.amount1008)} (
        <span className={colorizeVal(d1008)}>{d1008f}</span>)
      </td>
      <td className={borderStyle + " text-right"}>
        ⚡{formatter.format(miner.amount4320)} (
        <span className={colorizeVal(d4320)}>{d4320f}</span>)
      </td>
      <td className={borderStyle + " text-right"}>
        ⚡{formatter.format(miner.amountTotal)}
      </td>
    </tr>
  );
};

export default function TopMiners() {
  const [topMiners, setTopMiners] = useState<TopMiner[]>();
  const [sortKey, setSortKey] = useState<TopMinerSortKey>("amountTotal");
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("desc");

  useEffect(() => {
    fetch(
      "/api/topMiners?" +
        new URLSearchParams({ sort: sortKey, order: sortOrder })
    )
      .then((r) => r.json())
      .then((topMiners) => setTopMiners(topMiners))
      .catch(console.error);
  }, [sortKey, sortOrder]);

  return (
    <table className="w-full table-fixed border-collapse border border-white bg-neutral-600">
      <colgroup>
        <col className="w-12 sm:w-14 md:w-16" />
      </colgroup>
      <thead>
        <tr>
          <th className={borderStyle}>Rank</th>
          <th className={borderStyle}>Name</th>
          <SortableTableHeader
            label="1D"
            className={borderStyle}
            sortKey="amount144"
            currentSortKey={sortKey}
            sortOrder={sortOrder}
            setSortKey={setSortKey}
            setSortOrder={setSortOrder}
          />
          <SortableTableHeader
            label="7D"
            className={borderStyle}
            sortKey="amount1008"
            currentSortKey={sortKey}
            sortOrder={sortOrder}
            setSortKey={setSortKey}
            setSortOrder={setSortOrder}
          />
          <SortableTableHeader
            label="30D"
            className={borderStyle}
            sortKey="amount4320"
            currentSortKey={sortKey}
            sortOrder={sortOrder}
            setSortKey={setSortKey}
            setSortOrder={setSortOrder}
          />
          <SortableTableHeader
            label="Total"
            className={borderStyle}
            currentSortKey={sortKey}
            sortKey="amountTotal"
            sortOrder={sortOrder}
            setSortKey={setSortKey}
            setSortOrder={setSortOrder}
          />
        </tr>
      </thead>
      <tbody>
        <>
          {topMiners ? topMiners.map((miner, i) => minerToRow(miner, i)) : null}
        </>
      </tbody>
    </table>
  );
}
