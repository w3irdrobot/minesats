import Head from "next/head";

import Header from "@components/Header";

type PageProps = {
  children?: JSX.Element;
};
export default function Page({ children }: PageProps) {
  return (
    <>
      <Head>
        <title>minesats.gg</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="flex items-center">
        <div className="flex flex-col justify-center w-full">
          <Header />
          <div className="m-auto min-w-min px-1">{children}</div>
        </div>
      </div>
    </>
  );
}
