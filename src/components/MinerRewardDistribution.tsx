import React from "react";
import { Line } from "react-chartjs-2";
import _ from "lodash";

import { BlockReward } from "@/api/blockrewards";
import { useBlockRewards } from "@context/BlockRewardContext";
import { useColor } from "@context/ColorContext";

type AxisData = {
  label: string;
  rel: number[];
  abs: number[];
};

const parseBlockRewards = (blockRewards: BlockReward[]): AxisData[] => {
  const allNames = new Set(blockRewards.map(({ name }) => name));
  const objData: Record<string, { rel: number; abs: number }[]> = {};
  allNames.forEach((name) => {
    objData[name] = [];
  });
  let total = 0;
  for (const blockReward of blockRewards) {
    const { name, amount } = blockReward;
    let newTotal = total + amount;
    allNames.forEach((n) => {
      const previousValues = objData[n];
      const prevVal = previousValues[previousValues.length - 1];
      const prevAbs = prevVal?.abs ?? 0;
      const newAbs = prevAbs + (name === n ? amount : 0);
      const newRel = newAbs / newTotal;
      objData[n].push({ rel: newRel, abs: newAbs });
    });
    total = newTotal;
  }

  return Object.entries(objData).map(([k, v]) => ({
    label: k,
    rel: _.map(v, "rel"),
    abs: _.map(v, "abs"),
  }));
};

export default function MinerRewardDistribution() {
  const pickColor = useColor();
  const { val: blockRewardsDesc } = useBlockRewards();
  const blockRewardsAsc = [...blockRewardsDesc].reverse();
  const axisData = parseBlockRewards(blockRewardsAsc);
  const satsFormatter = new Intl.NumberFormat(`en`);

  const labels = Array.from(Array(axisData?.[0]?.abs.length || 0).keys());
  const data = {
    labels,
    datasets:
      axisData?.map(({ label, rel }) => {
        const color = pickColor(label);
        return {
          fill: true,
          label,
          data: rel.map((v) => v * 100),
          borderColor: color,
          backgroundColor: color,
        };
      }) || [],
  };
  const options = {
    legend: {
      labels: {
        fontColor: "#ffffff",
      },
    },
    responsive: true,
    interaction: {
      axis: "x" as "x",
      intersect: false,
    },
    scales: {
      y: {
        title: {
          display: true,
          text: "percentage",
          color: "white",
        },
        max: 100,
        min: 0,
        stacked: true,
        grid: {
          color: "grey",
          tickColor: "white",
        },
        ticks: {
          color: "white",
        },
      },
      x: {
        title: {
          display: true,
          text: "blocks",
          color: "white",
        },
        grid: {
          color: "grey",
          tickColor: "white",
        },
        ticks: {
          color: "white",
        },
      },
    },
    plugins: {
      title: {
        display: true,
        text: "Miner reward distribution",
        color: "white",
      },
      tooltip: {
        itemSort: (a, b) => b.raw - a.raw,
        callbacks: {
          label: function (context) {
            // Show absolute value of rewards in tooltip
            const name = context.dataset.label;
            const rel = context.parsed.y;
            const index = context.dataIndex;
            const abs =
              _.find(axisData, ({ label }) => label === name)?.abs[index] ?? 0;
            return (
              `${name}: ⚡${satsFormatter.format(abs)} ` +
              `(${rel.toFixed(2)}%)`
            );
          },
        },
      },
      legend: {
        labels: {
          color: "white",
        },
      },
    },
  };

  return <Line options={options} redraw={true} data={data} />;
}
