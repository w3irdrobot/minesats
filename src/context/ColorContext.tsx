import _ from "lodash";
import { createContext, useContext } from "react";

const colors = [
  "#B80C09", // engineering orange
  "#0B4F6C", // indigo dye
  "#01BAEF", // process cyan
  "#FBFBFF", // ghost white
  "#040F16", // rich black
  "#59A96A", // jade
  "#9BDEAC", // celadon
  "#15B097", // keppel
  "#71F79F", // light green
  "#3DD6D0", // robin egg blue
  "#513C2C", // café noir
  "#28190E", // bistre
  "#E9B44C", // hunyadi yellow
  "#9B2915", // rufous
  "#50A2A7", // verdigris
  "#A4243B", // amaranth purple
  "#273E47", // charcoal
  "#893168", // byzantium
  "#2E1C2B", // dark purple
  "#E0FF4F", // chartreuse
  "#FF6663", // bittersweet
  "#A51080", // fandango
];

const picks = {};

const pickColor = (key: string): string => {
  if (!_.has(picks, key)) picks[key] = _.sample(colors);
  return picks[key];
};

const ColorContext = createContext<(key: string) => string>(pickColor);

export const ColorContextProvider = ({ children }) => {
  return (
    <ColorContext.Provider value={pickColor}>{children}</ColorContext.Provider>
  );
};

export const useColor = () => useContext(ColorContext);

export default ColorContextProvider;
