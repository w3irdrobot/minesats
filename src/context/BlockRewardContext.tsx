import { useState, createContext, useEffect, useContext } from "react";
import { BlockReward } from "@/api/blockrewards";

const BlockRewardContext = createContext<{
  val: BlockReward[];
  update: () => Promise<void>;
}>(null);

export const BlockRewardProvider = ({ children }) => {
  const [blockRewards, setBlockRewards] = useState<BlockReward[]>([]);

  const update = () =>
    fetch("/api/blockrewards?limit=600&sort=desc")
      .then((r) => r.json())
      .then((blockRewards) => setBlockRewards(blockRewards))
      .catch(console.error);

  useEffect(() => {
    update();
  }, []);

  return (
    <BlockRewardContext.Provider value={{ val: blockRewards, update }}>
      {children}
    </BlockRewardContext.Provider>
  );
};

export const useBlockRewards = () => useContext(BlockRewardContext);

export default BlockRewardProvider;
