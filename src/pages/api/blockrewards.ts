// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

import conn from "@/api/_db";

export type BlockReward = {
  ts: number;
  name: string;
  amount: number;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<BlockReward[]>
) {
  const q = conn
    .select(["m.id", "m.ts", "br.name", "br.amount"])
    .from("blockrewards AS br")
    .join("messages AS m", "m.id", "br.id");

  if (req.query.after) {
    q.whereRaw(
      "m.ts > to_timestamp(?)",
      parseInt(req.query.after as string, 10)
    );
  }
  if (req.query.before) {
    q.whereRaw(
      "m.ts < to_timestamp(?)",
      parseInt(req.query.before as string, 10)
    );
  }

  let limit = 600;
  if (req.query.limit) limit = parseInt(req.query.limit as string, 10);
  q.limit(Math.min(limit, 600));

  let orderBy = "asc";
  if (req.query.sort) orderBy = req.query.sort === "asc" ? "asc" : "desc";
  q.orderBy("m.ts", orderBy);

  const rows = await q;

  res.status(200).json(
    rows.map(({ ts, amount, ...row }) => ({
      ts: +ts,
      amount: parseInt(amount, 10),
      ...row,
    }))
  );
}
