import knex, { Knex } from "knex";

const connectDB = (): Knex => {
  const conn = knex({
    client: "postgres",
    connection: {
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PW,
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_POR || "5432", 10),
      database: process.env.DATABASE_NAME,
    },
  });
  conn.on("query", (q) => {
    // console.log({ sql: q.sql, bindings: q.bindings });
  });
  return conn;
};

let conn: Knex;

if (process.env.NODE_ENV === "production") {
  conn = connectDB();
} else {
  if (!global.db) {
    global.db = connectDB();
  }
  conn = global.db;
}

export default conn;
