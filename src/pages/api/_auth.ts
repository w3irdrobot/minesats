import _ from "lodash";
import { NextApiRequest, NextApiResponse } from "next";
import secp256k1 from "secp256k1";

import db from "@/api/_db";

const HOUR = 1000 * 60 * 60;

export type AuthType = "login" | "signup";
type AuthResponse = { status: "OK" } | { status: "ERROR"; reason: string };

export default function authHandler(type: AuthType) {
  return async (
    { query }: NextApiRequest,
    res: NextApiResponse<AuthResponse>
  ) => {
    const error = (reason) => res.status(400).json({ status: "ERROR", reason });

    if (!query.k1) return error("no k1 provided");
    if (!query.sig) return error("no sig provided");
    if (!query.key) return error("no key provided");

    const sig = Uint8Array.from(Buffer.from(query.sig as string, "hex"));
    const k1 = Uint8Array.from(Buffer.from(query.k1 as string, "hex"));
    const key = Uint8Array.from(Buffer.from(query.key as string, "hex"));
    const signature = secp256k1.signatureImport(sig);
    const verify = secp256k1.ecdsaVerify(signature, k1, key);
    if (!verify) return error("invalid sig");

    const row = await db
      .select("*")
      .from("auth")
      .where({ k1: query.k1 })
      .first();
    if (_.isEmpty(row)) return error("unknown k1");

    const expired = Date.now() < +row.created - HOUR;
    if (expired) return error("k1 expired");

    // Check if pubkey already exists
    const user = await db
      .select("*")
      .from("users")
      .where({ pubkey: query.key })
      .first();
    if (type === "login" && _.isEmpty(user)) return error("unknown pubkey");
    if (type === "signup" && !_.isEmpty(user))
      return error("pubkey already exists");

    await db.raw(
      `
      INSERT INTO users(pubkey, session_id)
      SELECT ?, session_id
      FROM auth
      WHERE k1 = ?
      ON CONFLICT(pubkey) DO UPDATE SET session_id = EXCLUDED.session_id
    `,
      [query.key, query.k1]
    );
    await db.delete().from("auth").where({ k1: query.k1 });

    return res.json({ status: "OK" });
  };
}
