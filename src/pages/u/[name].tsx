import { useState } from "react";
import _ from "lodash";
import prettyMilliseconds from "pretty-ms";

import Page from "@components/Page";
import db from "@/api/_db";
import CopyText from "@components/CopyText";
import { useAuth } from "@context/AuthContext";

export async function getServerSideProps(ctx) {
  const { name } = ctx.query;

  const user = await db
    .select([
      db.raw(`COALESCE(player_name, pubkey) AS name`),
      "verify_token",
      "pubkey",
      "last_logout",
    ])
    .from("users")
    .leftJoin("players", "players.name", "users.player_name")
    .where({ player_name: name })
    .orWhere({ pubkey: name })
    .first();

  if (_.isEmpty(user)) {
    return { notFound: true };
  }

  if (user.last_logout) {
    // Only JSON serializable data can be returned so we cast date to number
    user.last_logout = +user.last_logout;
    // milliseconds since last logout
    user.last_logout_relative = Date.now() - user.last_logout;
    // One emerald is consumed after 20k ticks.
    // Server tickrate is 20 ticks per second.
    // An ASIC holds 64 emeralds.
    //   => 0.05 seconds/tick * 20,000 ticks * 64 = 64,000 seconds
    const REFILL_TIME = 64000000; // milliseconds
    user.refill_ts = user.last_logout + REFILL_TIME;
    user.refill_ts_relative = user.refill_ts - Date.now();
  }

  return {
    props: user,
  };
}

type UserProps = {
  name: string;
  verify_token: string;
  pubkey: string;
  last_logout?: number;
  last_logout_relative?: number;
  // Timestamp at which user needs to refill ASICs
  refill_ts?: number;
  refill_ts_relative?: number;
};

export default function User(user: UserProps) {
  const { user: me } = useAuth();
  const [showRelativeLastSeen, setShowRelativeLastSeen] = useState(true);
  const [showRelativeRefill, setShowRelativeRefill] = useState(true);

  const isMe = me?.pubkey === user.pubkey;

  return (
    <Page>
      <div className="grid grid-cols-1">
        <div className="mr-auto my-1">Last seen</div>
        <div
          className="flex m-auto text-button"
          onClick={() => setShowRelativeLastSeen((rel) => !rel)}
        >
          {user.last_logout
            ? showRelativeLastSeen
              ? prettyMilliseconds(user.last_logout_relative)
              : new Date(user.last_logout).toISOString()
            : "n/a"}
        </div>
        <div className="mr-auto my-1">Refill required</div>
        <div
          className="flex m-auto text-button"
          onClick={() => setShowRelativeRefill((rel) => !rel)}
        >
          {user.last_logout
            ? showRelativeRefill
              ? prettyMilliseconds(user.refill_ts_relative)
              : new Date(user.refill_ts).toISOString()
            : "n/a"}
        </div>
        {isMe ? (
          <>
            <div className="mr-auto my-1">Verify token</div>
            <div className="flex m-auto">
              <CopyText text={user.verify_token} />
            </div>
          </>
        ) : null}
      </div>
    </Page>
  );
}
