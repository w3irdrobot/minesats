import "@styles/globals.css";
import type { AppProps } from "next/app";
import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  Title,
  Tooltip,
  Legend,
  PointElement,
  LineElement,
  Filler,
} from "chart.js";
import PlausibleProvider from "next-plausible";
import { BlockRewardProvider } from "@context/BlockRewardContext";
import { NextBlockTimerProvider } from "@context/NextBlockTimerContext";
import AuthContextProvider from "@context/AuthContext";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler
);

export default function App({ Component, pageProps }: AppProps) {
  return (
    <PlausibleProvider domain="minesats.gg">
      <AuthContextProvider>
        <BlockRewardProvider>
          <NextBlockTimerProvider>
            <Component {...pageProps} />
          </NextBlockTimerProvider>
        </BlockRewardProvider>
      </AuthContextProvider>
    </PlausibleProvider>
  );
}
