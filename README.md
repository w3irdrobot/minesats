# minesats

Inofficial player stats and more for [Satlantis](https://satlantis.net/).

![](./img/2023-03-04-113759_1920x1080_scrot.png)

Website: minesats.gg

## Development

This project consists of two parts:

- bot in golang
- website using NextJS

### Bot setup

1. Start postgres container with initial credentials given in `docker-compose.yml`:

```txt
$ docker-compose up -d postgres
```

2. `cd` into `bot/` and copy `.env.template` to `.env` and configure `BOT_TOKEN` to use your discord token.
   This can also be your user token if you have no bot in the server but keep in mind that "self-bots" are [against Discord ToS](https://support.discord.com/hc/en-us/articles/115002192352-Automated-user-accounts-self-bots-).
   You should be able to find your user token in the network requests made when logging into Discord.
   If you have trouble finding it, let me know and I'll update this description.

3. Join Satlantis discord if you haven't yet since you will need to see the `#live-action` channel: https://discord.com/invite/CqyGY339xp
4. Run the bot with `go run .`. It should start to fetch all messages from the beginning of the channel, insert them into the database and parse them.

### Website setup

Install app dependencies and run website. Database must be up.

```txt
$ yarn install
$ yarn run dev
```

### Remote development

HTTPS is required for LNURL-auth. This makes it hard to run the website locally since login/signup doesn't work.
However, if a remote server is configured with SSL and the website can be accessed under dev1.minesats.gg, we can use `inotifywait` and `rsync` to sync local changes to the remote server:

```txt
inotifywait -r -m -e close_write src/ --format '%w%f' | while read MODFILE
do
        rsync $MODFILE dev1.minesats.gg:minesats/$MODFILE
done
```

If the website is run using `next dev`, the code will be recompiled on every change.
